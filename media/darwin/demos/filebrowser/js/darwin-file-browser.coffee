#--- DARWIN FILE BROWSER ---

darwinConfig = { appId: 'd147c9e5d875', autoSignIn: true};
cdrive =""

# -- Id's --
String::hashCode = ->
  hash = 0
  i = undefined
  chr = undefined
  len = undefined
  return hash  if @length is 0
  i = 0
  len = @length

  while i < len
    chr = @charCodeAt(i)
    hash = (hash << 5) - hash + chr
    hash |= 0
    i++
  hash
  
hash = (f) ->
  "h" + f.toString().hashCode()
  

filebrowser_idmap = {}

#---Darwin file browser clipboard---
window.clipboard =
  id: ""
  name: ""
  path: ""
  data: ""

pasteFile = (dest_path, paste_data) ->
  # open the current directory
  dest_dir = d_getDirectoryObject(dest_path)
  dest_dir.openFile(clipboard.name).then ((response) ->
    response.write(paste_data).then ((next_response) ->
      $("#darwin-file-explorer").treegrid "reload" 
    ), ->
      console.log('paste error!')
      return
  )

duplicateFileInNewLoc = (currentEncDir) ->
  console.log(clipboard.data)
  pasteFile(currentEncDir, clipboard.data)
  return

copyFileData = (currentEncDir, callback) -> 
  copy_path = clipboard.path.replace("/" + clipboard.name, "")
  copy_dir = d_getDirectoryObject(copy_path)
  copy_dir.openFile(clipboard.name).then ((data) ->
    data.read().then((copy_data) ->
      clipboard.data = copy_data
    )
  ), ->
    console.log "copy error!"


#reload the file explorer UI after a change
reloadCallback = () ->
  $("#darwin-file-explorer").treegrid "reload" 
  return


#UI buttons
d_attach_event_handlers = ->
  $('.new-folder-button').on "click", ->
    dialog = new NewFolderDialog($("#dialog"), (name) ->
      d_createFolder cdrive, name, reloadCallback
      return
    )  
  $('.new-upload-button').on "click", ->
    dialog = new NewUploadDialog($("#upload"), (file_data, file_name) ->
      d_uploadFile cdrive, file_data, file_name
      return
    )
  return


d_getDirectoryObject = (path, callback) ->
  console.log "invalid path"  unless path
  path = path.substring(1)  while (path.length) and (path[0] is "/")
  unless path.length
    console.log "invalid path"
    return -1
  patharray = path.split("/")
  darwinApp.getDrive(patharray[0]).then((top_level_drive) ->
    dest_dir = top_level_drive.getRoot();
    #if there are some directory-level to move down...
    i = 1
    while i < patharray.length
      dest_dir = dest_dir.getDirectory(patharray[i])
      i++
      console.log(dest_dir) 
    callback dest_dir  
     
  )
  
 
d_renameItem = () ->
  #copy and paste into new location with new file name
  
d_uploadFile = (row_path, data, name) ->
  d_getDirectoryObject(row_path, (dest_dir)->
    dest_dir.openFile(name, true).then ((response) -> #open a new image file
      response.write(data).then (-> 
        $("#darwin-file-explorer").treegrid "reload" 
        console.log('reloaded')
        return
      ), (-> #error handling
        alert "Error while writing file"
        return
        )
      return
    ), (->
        console.log "error!"
        return
      )
    return
  )
  

d_createFolderInEnclosingDir = (full_path, name, callback) ->
  splitpath = full_path.split("/")
  enclosing_directory_path = full_path.replace("/" + splitpath[splitpath.length - 1], "")
  d_createFolder enclosing_directory_path, name, callback
  return

d_createFolder = (path, name, callback) ->
  d_getDirectoryObject(path, (dd)->
    dd.getDirectory name
    callback()
    return
  )


#--IMAGE PREVIEW: displays an image in the preview--//
darwinTextPreview = (path, remoteFile) ->
  dpath = path.replace("/" + remoteFile, "")
  d_getDirectoryObject(dpath (dest_dir)->
    #get the root of the remote file storage    
    fileToPreview = remoteFile  
    #use the value as input to the darwin file storage
    dest_dir.openFile(fileToPreview).then ((remoteLoc) ->
      remoteLoc.read().then ((remoteImg) ->       
        #get the data  and fill the src tag
        data_string = JSON.stringify(remoteImg)
        $(".darwin-file-preview").innertext data_string
        $(".darwin-file-preview").show();
        return
      ), (msg) ->
      console.log "error getting src data" + msg
      return

  ), (msg) ->
      console.log "--error opening image--" 
      return    
    return                       
  )
  
  
#--IMAGE PREVIEW: displays an image in the preview--//
darwinImagePreview = (path, remoteFile) ->
  dpath = path.replace("/" + remoteFile, "")
  d_getDirectoryObject(dpath, (dest_dir)->
    #get the root of the remote file storage    
    fileToPreview = remoteFile
    #use the value as input to the darwin file storage
    dest_dir.openFile(fileToPreview).then ((remoteLoc) ->
      remoteLoc.read().then ((remoteImg) ->    
        #get the data  and fill the src tag
        encoded_string = "data:image/png;base64," + btoa(String.fromCharCode.apply(null, remoteImg));
        $(".darwin-file-preview").attr "src", encoded_string
        $(".darwin-file-preview").show();
        return
      ), (msg) ->
      console.log "error getting src data" + msg
      return   
      return
    ), (msg) ->
        console.log "--error opening image--" 
        return
      
      return                                  
  )
  


#DELETE: Delete a file or directory from the remote storage --//
darwinDelete = (file, path) ->
  short_path = path.replace('/' + file.name , "");
    
  d_getDirectoryObject(short_path, (dest_dir)->
    dest_dir.deleteDirectoryRecursively(file.name).then (response) ->
        $("#darwin-file-explorer").treegrid "reload"
        return

      dest_dir.deleteFile(file.name).then (response) ->
        $("#darwin-file-explorer").treegrid "reload"
        return

      return                       

  )


#GET IMAGE DATA: Get data for a specified image--//
darwinGetImageData = (currentPath, path, remoteFile, callback) ->
  root = darwinApp.getDrive(path)
  
  #get the root of the remote file storagedarwinApp.getFileStorage(path).getRoot();
  fileToPreview = remoteFile
  
  #use the value as input to the darwin file storage
  root.openImage(fileToPreview).then ((remoteLoc) ->
    remoteLoc.read().then (remoteImg) ->
      
      #read image          
      srcData = remoteImg
      
      #get the data  and fill the src tag
      darwinAddImage currentPath, clipboard.name, srcData
      return

    return
  ), ->
    console.log "error opening image"
    return

  return


#GET DIRECTORY CONTENT
getDirectoryContent = (path, callback) ->
  
  #Get Directory content function
  d_getDirectoryObject(path, (dest_dir)->
    contents = {} 
    #array-like object to hold the file objects
    dest_dir.listAllFileNames().then ((response) ->
      contents["files"] = response
      if contents["directories"] isnt `undefined`
        callback contents, path
      else
        console.log ("no directories");
        return
    ), ->
      alert "Error listing files!"
      return
  
    dest_dir.listAllDirectoryNames().then ((response) ->
      console.log(response)
      contents["directories"] = response
      if contents["files"] isnt `undefined`
        callback contents, path
      else
        alert "no files"
      return
    ), ->
      alert "Error listing directories!"
    return

    return

 )

 
#dialog UI
NewFolderDialog = (jquery_selector, success) ->
  self = this
  @object = jquery_selector
  @success = success
  @on_init self
  this

NewFolderDialog::on_submit_click = ->
  @success @object.find(".folder_name_input").val()
  @object.dialog "close"
  @object.find(".submit").off()
  
  return

NewFolderDialog::on_init = ->
  self = this
  # $('#dialog').dialog('title','New Folder');
  $("#dialog").dialog "open"
  $("#dialog").css "display", "inline"
  @object.find(".submit").on "click", ->
    self.on_submit_click()
    return

  return

RenameItemDialog = (jquery_selector, success) ->
  self = this
  @object = jquery_selector
  @success = success
  @on_init self
  this

RenameItemDialog::on_submit_click = ->
  
  @success @object.find(".rename-item-input").val()
  @object.dialog "close"
  @object.find(".submit").off()
  
  return

RenameItemDialog::on_init = ->
  self = this
  # $('#dialog').dialog('title','New Folder');
  $("#rename").dialog "open"
  $("#rename").css "display", "inline"
  @object.find(".submit").on "click", ->
    self.on_submit_click()
    return

  return


#UPLOAD
NewUploadDialog = (jquery_selector, success) ->
  self = this
  @object = jquery_selector
  @success = success
  @on_init self
  this
  
NewUploadDialog::on_init = ->
  self = this
  $("#upload").css "display", ""
  $("#upload").dialog "open"
  
  @object.find(".upload-file").on "click", ->
    self.on_upload_click()
    return

  return

NewUploadDialog::on_upload_click = ->
  self = this
  
  #get references to the file input
  fileInput = document.getElementById("fileInput")
  file = fileInput.files[0]
  
  reader = new FileReader()
  reader.onload = (e) -> 
    file_data = new Uint8Array(reader.result)  
    self.success file_data, file.name
    return
  
  reader.readAsArrayBuffer file
  
  @object.dialog "close"
  @object.find(".submit").off()
  
  return


#INIT FILEMANAGER
init_filemanager = ->

  $("#darwin-drive-explorer").treegrid
  
    idField:'id',
    nameField:'name',
    
    columns: [[{title: "", field: "permissions"},{title: "Drive", field: "name"} ]]
    
    onClickCell: (f,r) ->
      cdrive = r.name
      $("#darwin-file-explorer").treegrid "reload" 
      $(".file-explorer-buttons").show()
      
      
    loader: (param, success, error) ->
      ddata =[]
      
      window.darwinApp.getAllDriveNames().then ((drives) -> 

        for elem in drives
          modname = elem
          
#           if modname is 'images'
#             modname = '<i class="ion-images"></i>&nbsp;images'
#             
#           if modname is 'profile'
#             modname = '<i class="ion-person"></i>&nbsp;profile'
            
          ddata.push
            id: 'fake',
            name: modname
            permissions: '<i class="ion-person-stalker"></i>&nbsp;<i class="ion-unlocked"></i>'
        success ddata
      )

  console.log('--- init filemanager called ---')
  #DARWIN TREEGRID
  $("#darwin-file-explorer").treegrid
    columns: [[
      title: "Root Directory"
      field: "name"
    ]]
 
    #on load...
    loader: (param, success, error) ->
      unless param["id"]?
        ndata = []
        unless cdrive is ""    
          path = cdrive
          getDirectoryContent path, (data, path) ->
        
            for k of data["files"]
              idv = hash(path + "/" + k)
              fp = "/" + path + "/" + data["files"][k]
              filebrowser_idmap[idv] = fp
              ndata.push
                id: idv
                fullpath: fp
                name: data["files"][k]

            for k of data["directories"]
                idv = hash(path + "/" + k)
                fp = "/" + path + "/" + data["directories"][k]           
                filebrowser_idmap[idv] = fp
                ndata.push
                  id: idv
                  fullpath: fp
                  name: data["directories"][k]
                  state: "closed"
            success ndata

        success ndata
      else
      
        getDirectoryContent filebrowser_idmap[param["id"]], callback = (data, path) ->
          console.log('--- re loader started ---')

          ndata = []
          for k of data["files"]
            idv = hash(path + "/" + k)
            fp = path + "/" + data["files"][k]
            filebrowser_idmap[idv] = fp
            ndata.push
              id: idv
              fullpath: fp
              name: data["files"][k]

          for k of data["directories"]
            idv = hash(path + "/" + k)
            fp = path + "/" + data["directories"][k]
            filebrowser_idmap[idv] = fp
            ndata.push
              id: idv
              fullpath: fp
              name: data["directories"][k]
              state: "closed"

          success ndata
          return

      return

    idField: "id"
    treeField: "name"
    
    #RIGHT CLICK ON ITEM
    onContextMenu: (e, row) ->
      e.preventDefault()
       $(this).treegrid "select", row.id
      $("#browser-context-menu").menu "show",
        left: e.pageX
        top: e.pageY
        onClick: (item) ->
          active_item = row.name
          clicked_menu_item = item.text
          
          if clicked_menu_item is "New Folder"
            newfoldertargetpath = row.fullpath
            dialog = new NewFolderDialog($("#dialog"), (name) ->
              if row.name.indexOf(".") is -1
                # "folder"            
                d_createFolder newfoldertargetpath, name, reloadCallback                
              else
                # "file"             
                d_createFolderInEnclosingDir newfoldertargetpath, name, reloadCallback

              return
            )

          if clicked_menu_item is "Rename"
            console.log('--- renaming ---')
	    #copy original file
            clipboard.id = row.id
            clipboard.name = row.name
            clipboard.path = row.fullpath
            copyFileData()
            dialog = new RenameItemDialog($("#rename"), (new_name) ->
              #delete original file
              darwinDelete row, row.fullpath 
              #duplicate file with new name
              clipboard.name = new_name
              duplicateFileInNewLoc row.fullpath.replace("/" + row.name, "")
              $(".darwin-file-preview").show();

            )
            
          if clicked_menu_item is "Upload File"
            newfilefullpath = row.fullpath
            dialog = new NewUploadDialog($("#upload"), (file_data, file_name) ->
              if row.name.indexOf(".") is -1
                console.log "folder"
              else
                console.log "file"
              d_uploadFile row.fullpath, file_data, file_name
              return
            )
          
          if clicked_menu_item is "Copy"   
            children = $("#darwin-file-explorer").treegrid "getChildren", row.id
            #copy the id of the item
            clipboard.id = row.id
            clipboard.name = row.name
            clipboard.path = row.fullpath
            copyFileData()
          if clicked_menu_item is "Paste"
            duplicateFileInNewLoc row.fullpath   
            imagedata = ""
          if clicked_menu_item is "Delete"
            darwinDelete row, row.fullpath,  
          return

      return

    
    #LEFT CLICK
    onClickRow: (row, event) ->
      #if it is a image...
      #...display image previewl
      if row.name.indexOf(".png") > -1 or row.name.indexOf(".jpg") > -1
        darwinImagePreview JSON.stringify(row.fullpath).replace(/\"/g, ""), JSON.stringify(row.name).replace(/\"/g, "")  if row.name.indexOf(".png") > -1 or row.name.indexOf(".jpg") > -1
      else #assume it is a text document
        # darwinTextPreview JSON.stringify(row.fullpath).replace(/\"/g, ""), JSON.stringify(row.name).replace(/\"/g, "")
      return

  d_attach_event_handlers()
  return


removeSplashPage = ->
  $('.darwin-splash').fadeOut('1000')

#DARWIN FILE BROWSER starts when page is ready
$(document).on "ready", ->
  $('.file-explorer-buttons').hide()
  #new DARWIN Client with config
  darwinClient = new window.darwin.Client(darwinConfig)
  window.darwinClient=darwinClient
  #start DARWIN Client
  darwinClient.start().then(
    ->
      #init connection with the client and the server   
      window.darwinApp = darwinClient.getApplication()
      darwinClient.signIn().then(->
         alert("signed in!")  
      )

      darwinClient.registerEventHandler('sign-in-success', ()->
        removeSplashPage()
      
        darwinClient.registerEventHandler "drives-initialised", ->
        
          init_filemanager()
        
      
      
      
      
      
      
      )
      

      return
      return 
    , (e)-> alert('ERROR: ' + e)
    ).catch(
      (e)-> alert('ERROR: ' + e)
    )
  
  return


