
darwinConfig = { appId: 'f148f06ddc2d', autoSignIn: true};
current_nodes = []
current_edges = []
graph = {
  nodes: []
  edges: []
  currentlyselected: "",
  data: ""
}

view = {
  incoming_load_list: []
  outgoing_load_list: []
  currently_selected_node: -1
}

settings = {
  MAX_NODES_PERC : 10 
}


map_object = (object, prefix, inserter )  ->
  for k in _.keys(object) 
    v=object[k]
    if typeof(v) == 'object'
      map_object(v,prefix+"."+k,inserter)
    else
      inserter(prefix+"."+k,v)
  return

map_into_propery_grid = (object, pg) ->
   map_object(object,"", (k,v) -> 
    pg.pushs
      name: k,
      value: v
   )

add_menu_item = (new_menu_item) ->
  console.log('adding item...')
  console.log(new_menu_item)
  graph_menu_array.push
    id: new_menu_item
    text: new_menu_item
    data: JSON.parse(new_menu_item)
  
  $('.graphdropdown').combobox('reload');
  
menu_item_click = ->
  console.log('!')


load_graphs = ->
  k = 0
  for i in graph_menu_array
    add_menu_item(graph_menu_array[k])
    k++
    

load_selected_graph_data =  (selected_item) ->
  graph.currentlyselected = item.text
  graph.data = graph_menu_array[item.text]

  for obj in graph_menu_array
    if obj.id is item.id
      graph.data = obj.data
      

removeSplashPage = ->
 $('.darwin-splash').fadeOut('1000');


functions = {
  nodeExists: 'function() { success(true); }',
  enumerateNodeIds: 'function(id) { if (isNaN(id)) { success(0); } else { if (id < 10) { success(id + 1); } else { error(); } } }',
  enumerateNodeInboundEdgeId: 'function() { success(null); }',
  enumerateNodeOutboundEdgeIds: 'function(id) { success([ (id+1%10),  (id+2%10)  ]); }',
  getNodeAttributes: 'function(id) { success({color: "#f00;"}); }',
  getEdgeAttributes: 'function() { success({}); }'
};


gotNodes = ()->
  alert('got nodes' + current_nodes)
   
 
gotEdges = ()->
  alert('got edges' + current_edges)
   
 
 
getNextNode = (nodeEnumerator)->  
  console.log('--- get next node called ---')  
  nodeEnumerator.hasNext().then(()-> # Checks whether there is next node in the collection                                        
    nodeEnumerator.next().then((node)->  # Gets the next node
      console.warn('Node:', node.getId()) # Prints the node                                                                              
      node.enumerateOutboundEdges().then((edgeEnumerator)->
        console.log('---enumerating outbound edges---')
        getNextEdge(edgeEnumerator)
      )
                                                                                      
      current_nodes.push(node.getId())
      getNextNode(nodeEnumerator);
    ,(err)->
      console.error('Failed to get next node!', err.message)
      buildUIGraph()
    )
  ,(err)-> 
    console.error('There is no next node to enumerate!', err.message)
    buildUIGraph()
  )    
                                                                                                                                                                              
getNextEdge = (edgeEnumerator)->  
  console.log('--- get next edge called ---')
  console.log(edgeEnumerator)
  edgeEnumerator.hasNext().then(()->  # Checks whether there is next node in the collection
    console.log('--- edge has next ---')
    edgeEnumerator.next().then((edge)-> 
      console.log('--- in next edge ---')
      console.log(edge)
      console.warn('Edge:', edge.getUuid()) 
      current_edges.push(edge.getUuid())
      getNextEdge(edgeEnumerator)
    ,(err)->
      console.error('Failed to get next edge!', err.message)
    )
  ,(err)-> 
    console.error('There is no next edge to enumerate!', err.message);
  )

                                                                                                                                                                                 
afterGraphCreate = (graph)-> 
  console.log(graph);
  console.warn('Graph ID =', graph.getId());                                                                                                                                                                                
  # Enumerates over some nodes
  graph.enumerateNodes().then((nodeEnumerator)-> # Gets node enumerator for our graph
    getNextNode(nodeEnumerator);
  ,(err)-> 
    console.error('Failed to get node enumerator!', err.message);
  )


#DARWIN FILE BROWSER starts when page is ready
$(document).on("ready", ->
  darwinClient = new window.darwin.Client(darwinConfig)
  window.darwinClient=darwinClient

  darwinClient.start().then( ->                      
    
    darwinClient.registerEventHandler('sign-in-success', ()->
      removeSplashPage()
    
      #init connection with the client and the server   
      window.darwinApp = darwinClient.getApplication()
    
    
      console.log('creating explicit graph');
      
      darwinClient.getImplicitGraph('cf4d449c-ba09-46fc-b40d-3817a01c57bc').then((graph)-> # Creates graph with defined functions.
        console.warn('Graph found')
        afterGraphCreate(graph)
      ,(err)-> 
        console.error('Failed to create a graph!', err.message);
      )

    

      window.darwinApp = darwinClient.getApplication()

      console.log window.darwinApp
    
    
    )#eo signin event handler
  
  )
)


spm_transition_at_p=(c,p)->
  if p!=(c.length-1)
    if (c[p]>(c[p+1]+1))
      r=c.slice(0)
      r[p+1]+=1
      r[p]-=1
      return r
    return null
  if (c[p]>1)
    r=c.concat([1])
    r[p]-=1
    return r
  return null

spm_edges_for_node=(n)-> 
  res=[]
  for p in [0... n.length]
    cr=spm_transition_at_p(n,p)
    if cr!=null
      res.push([n,cr])
  return res

spm_initial_node=[15]

chash=(c)->c.toString()



enumerate_nodes=(stnode,e4n,stack)->
  visited=[]
  hvisited=[]
  if (stack is undefined)   
    stack=[stnode]
    visited.push(stnode)
    hvisited.push(chash(stnode))
  while stack.length!=0
    cn=stack.pop()    
    for e in e4n(cn)
      ch=chash(e[1])
      if ch not in hvisited
        stack.push(e[1])
        visited.push(e[1])
        hvisited.push(ch)
  return visited

enumerate_edges=(nodes,e4n) ->
  res={}
  res.v=[]
  _.map(nodes,(cn)->
        res.v=res.v.concat(e4n(cn))
  )
  return res.v


buildUIGraph = ()->
  #orgn= [0,1,2,3,4,5,6,7]
 
  
  #_(orgn).map(
  #          )   
  console.log("A")
  current_nodes=orgn=enumerate_nodes(spm_initial_node,spm_edges_for_node)
  console.log("B")
  console.log(orgn)
  current_edges=orge=enumerate_edges(orgn,spm_edges_for_node)
  console.log("C")
  console.log(orge)
  #     functions.enumerateNodeIds((orgn) -> 
  #       orge=edge[]           
  #       _.map(orgn, n -> 
  #         functions.enumerateNodeOutboundEdgeIds(n, (le)->
  #           orge=orge.concat( _.map(n,oe))
  #       ))                             
  #     )
  
  #console.log(orgn) # SHOULD BE LIST OF OJBECTS WITH ids
  #console.log(orge) # SHOULD BE LIST OF OJBECTS WITH source target linking reference in node list
  d3n=_.map(current_nodes, (n) ->
            return {'cfg':n,'label':n.toString()}
          
          )
  
  #console.log(d3n) # SHOULD BE LIST OF OJBECTS WITH ids
  #alert(d3n)
  
  #console.log(current_edges)
  #console.log(orge)
  #alert(orge)
  
  orgnhash=_.map(orgn,chash)
  console.log(orgnhash) # SHOULD BE LIST OF OJBECTS WITH source target linking reference in node list
  
  d3e=_.map(orge, (e) ->
            return { 'source': d3n[orgn.indexOf(e[0])],'target': d3n[orgnhash.indexOf(chash(e[1]))]}
          ) 
  
  
  console.log(d3e) # SHOULD BE LIST OF OJBECTS WITH source target linking reference in node list
  
  #set width and height
  width = $(window).width()
  height = 500
  force = d3.layout.force()
  force.nodes(d3n).links(d3e)
  force.size([width, height])
  force.gravity(0.2).charge(-220).linkDistance(100)
  
  transform = (d) ->
    return "translate(" + d.x + "," + d.y + ")"


  force.on('tick' , ()->      
    #console.log('t')
    node
      .attr('cx', (d)-> return d.x)
      .attr('cy', (d)-> return d.y)
           
    text
      .attr('transform', (d)-> transform(d))
    
    link.attr('x1', (d)-> return d.source.x)
      .attr('y1', (d)-> return d.source.y)
      .attr('x2', (d)-> return d.target.x)
      .attr('y2', (d)-> return d.target.y)
 
  )  
      
  #svg container object
  svg = d3.select('body').append('svg')
    .attr("width", '100%')
    .attr("height", height)
  node = svg.selectAll('.node').data(d3n).enter().append('circle').attr('class', 'node').attr('r',5).call(force.drag)
  node.append("title").text("1")
  link = svg.selectAll('.link').data(d3e).enter().append('line').attr('class', 'link');
  
  text = svg.selectAll("text")
    .data(force.nodes())
    .enter().append("text")
    .attr("x", 8)
    .attr("y", ".20em")

    .text((d)-> return d.label );
  
  
  try
    force.start()
  catch error
    console.log(error)
    console.log(error.stack)
  throw e
