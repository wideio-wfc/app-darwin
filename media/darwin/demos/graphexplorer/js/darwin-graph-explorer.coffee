#--- DARWIN FILE BROWSER.JS ---
window.darwinpath = "images"
darwinConfig = { appId: 'f148f06ddc2d', autoSignIn: true};
dummy_data = {"a": {"b":2, "d":5}, "c":4}
graph_menu_array = [{"id":1,"text":"DARWIN demo Exp. Graph", "data": [[1,2,3], [[1,2], [2,3]]]}, 
                    {"id":2,"text":"[[4,2,8], [[4,2], [2,8]]]", "data": [[4,2,8], [[4,2], [2,8]]]}
                   ]
graph = {
  nodes: []
  edges: []
  currentlyselected: "",
  data: ""
}

view = {
  incoming_load_list: []
  outgoing_load_list: []
  currently_selected_node: -1
}

settings = {
  MAX_NODES_PERC : 10 
}


reloadviews = ()->
  console.log('--- reloading views ---')
  $('#pg').propertygrid("reload")
  $('#incominggrid').datagrid("reload")
  $('#outgoinggrid').datagrid("reload")
  
find_center_node_and_visualise = (center_node) ->
  visualise_graph_data(center_node, reloadviews)

visualise_graph_data = (center_node, reloadviews) ->
  #clear old data
  
  view.outgoing_load_list = []
  view.incoming_load_list = []
  
  view.currently_selected_node = center_node
  
  console.log('---visualising graph data---' + center_node)
  #generate outgoing list and incoming list
  #cycle through all the edges
  console.log(graph.edges)
  

  
  for k,v of graph.edges
    console.log('for kv: ' + v)
    if v[0] is center_node
      view.outgoing_load_list.push(v[1])
    if v[1] is center_node
      view.incoming_load_list.push(v[0])
    
  
  console.log('IN: ' + view.incoming_load_list)
  console.log('OUT: ' + view.outgoing_load_list)
  #reload the views
  
  reloadviews()
  
  
  
#generate_uid()

$('.add-uri').click ->
  if $('.uri-input').val() isnt ""
    add_menu_item($('.uri-input').val())
  
$('#creategraph').click ->
  reloadviews()
  
$('#downloadgraph').click ->
  console.log('--- download graph started ---')
  generate_graph(graph.data)
  
generate_graph = (graph_data) ->
  #clear old data
  graph.nodes = []
  graph.edges = []
  
  console.log('--- generating graph ---')
  console.log(graph_data)
  
  #for each item in the node array, create a node
  for i in graph_data[0]
    console.log('-> add node.' + i)
    #uid = generate_uid()
    graph.nodes[i] = i #TODO: add uid
  

  # for each item in the edge array. create an edge
  for i in graph_data[1]
    console.log('-> add edge.' + i)
    graph.edges[i] = i #TODO: add eid

  #display graph
  #add to loader
  find_center_node_and_visualise(2)
  
  
  console.log(graph.nodes)    
  console.log(graph.edges)
  #$('#pg').propertygrid('reload')

#console.log(nodelist["uid_test"].color)



map_object = (object, prefix, inserter )  ->
  for k in _.keys(object) 
    v=object[k]
    if typeof(v) == 'object'
      map_object(v,prefix+"."+k,inserter)
    else
      inserter(prefix+"."+k,v)
  return

map_into_propery_grid = (object, pg) ->
   map_object(object,"", (k,v) -> 
    pg.push
      name: k,
      value: v
   )

add_menu_item = (new_menu_item) ->
  console.log('adding item...')
  console.log(new_menu_item)
  graph_menu_array.push
    id: new_menu_item
    text: new_menu_item
    data: JSON.parse(new_menu_item)
  
  $('.graphdropdown').combobox('reload');
  
menu_item_click = ->
  console.log('!')


load_graphs = ->
  k = 0
  for i in graph_menu_array
    add_menu_item(graph_menu_array[k])
    k++
    

load_selected_graph_data =  (selected_item) ->
  graph.currentlyselected = item.text
  graph.data = graph_menu_array[item.text]

  for obj in graph_menu_array
    if obj.id is item.id
      graph.data = obj.data
      

init_graphexplorer = (g)->
  console.log('--- graph explorer init ---')
  #console.log(g)
 
  $('.graphdropdown').combobox({
    onSelect: (item) ->
      graph.currentlyselected = item.text
      graph.data = graph_menu_array[item.text]

      for obj in graph_menu_array
        if obj.id is item.id
          graph.data = obj.data

      generate_graph(graph.data)
      
#     valueField:'id',
#     textField:'text',
#    mode: 'local'
    loader: (param,success,error) ->
      console.log('--- graph menu array started ---')
      success graph_menu_array
      return
  })
 
  #auto-select the darwin demo graph
  $('.graphdropdown').combobox('setValue', '001');
  #load demo graph data
  generate_graph(graph_menu_array[0].data)
  find_center_node_and_visualise(2)
  
  
  console.log('got out of the func')


  $('#incominggrid').datagrid({
    fitColumns: true,
    
    columns:[[
      {field:'name',title:'name'}
    ]]
    
    onClickCell: (i,f,v) ->
      find_center_node_and_visualise(v)
      
      
    loader: (param, success, error) ->
       ndata = []
       for i in view.incoming_load_list
         ndata.push
           name: i
       success ndata
       return 
  });
  
  
  $('#outgoinggrid').datagrid({
    fitColumns: true,
    
    columns:[[
      {field:'name',title:'Name'},
      ]]
    
    onClickCell: (i,f,v) ->
      find_center_node_and_visualise(v)

    
    loader: (param,success,error) ->   
      ndata = []
      for i in view.outgoing_load_list
        ndata.push
          name: i
      success ndata
      return   
    
    
  });
    
  $('#pg').propertygrid({
      scrollbarSize: 0,
      
      loader: (param, success, error) ->
        data = []
        data.push
          name:   view.currently_selected_node
        #console.log(g)
        #console.log(g.getNodes()) # get nodes is not yet implemented??
        #console.log(nodelist)
        #for k of nodelist
        #   console.log('N:' + k)        
        #   map_into_propery_grid(nodelist[k], ndata)     
        success data
  })  




removeSplashPage = ->
 $('.darwin-splash').fadeOut('1000');
  

createGraph = ->
  darwinClient.createExplicitGraph().then ( (g) -> 
    graphId = g.getUuid()
    return graphId
  )


createGraphNode = (g)->
  n = g.createNode().then((n) ->
    console.log('^^^ creating test n ^^^')             
    
  )
  return n

createGraphNodes = (g, attr) ->
  promises =[]
  for elem in attr
    console.log('ce')
    promises.push(g.createNode(elem))
  console.log(promises)
  promises
  

#DARWIN FILE BROWSER starts when page is ready
$(document).on "ready", ->
  console.log('... on ready ...')
  #new DARWIN Client with config
  darwinClient = new window.darwin.Client(darwinConfig)
  window.darwinClient=darwinClient
  #start DARWIN Client
  darwinClient.start().then( ->
    console.log('... on start ...')                      
    
    darwinClient.registerEventHandler('sign-in-success', ()->removeSplashPage())
    
    #init connection with the client and the server   
    window.darwinApp = darwinClient.getApplication()
    
    window.graph = darwinClient.createExplicitGraph().then ( (g) ->                          

      init_graphexplorer(g)
                                                                                               
      console.log('--- explicit graph ---')
      

      console.log(g.getUuid());
      console.log(g)
      
      
      #createGraphNode(g)
      
      
      #init_graphexplorer()
      
     # console.log('--starting map--')      
     # pa = []
      nodeIds = [0,1,2]
      
 
      g.createNode(nodeIds).then((nodes)->
        for node of nodes
          console.log(nodes[node].getId())
      )

      
      #CREATEEDGE() IS NOT GIVING ANYTHING, NOT EVEN ERROR MESSAGE
      console.log('--- creating edge ---')
      g.createEdge('d8a54edd-289c-4139-b555-d0c7ed321343', '54ece489-ae7f-44a9-b56b-eba2d15f778c').then((edges)->
        console.log('edge created')
        console.log(edges)                                  
      ,(err)->console.log('create edge error: ' + err))    
 
 
 
      
      
     # console.log('nodeids:')
     # console.log(nodeIds)
      
      #s =() ->
      #  console.log('SSS!!!')
      #Promise.all(createGraphNodes(g, nodeIds)).then(s)
      
      
      
      
      #for i in nodeIds
       # console.log('for loop')
      #  pa.push(createGraphNode(g))
        
      #console.log('promises')
     # console.log(pa)

#       pa = _.map(
#         [0,1,2],
#         (i)-> 
#           window.graph.createNode().then((n) ->
#             console.log('mapping prom')
#             #console.log(n)
#             #return n
#             return n
#           
#           )
#           
#         #nodelist[i] = JSON.parse(dummy_data)
#         #nodelist[i].uuid = n.getUuid()
#         #console.log(nodelist[i].uuid)
#       )
#  
# #      )
#       console.log(pa)

      
      #fa=(node_results)->
       # console.log('--- FINISHED PROMISE MAP ---')
       # console.log(node_results) #we sould get an array to the promis objects
      
 #      console.log(node_list)
 #      fb=(error_obj)->
 #       console.log(error_obj)
      #Promise.all(pa).then(fa(n) ->)
            #console.log(g)
            
            #setup one demo connections
            #console.log('-- setting up nodes --')
            #console.log(g.getNode(nodelist[1].uuid))
           #g.getNode(nodelist[1].uuid).createEdge(nodelist[0].uuid)
            
            # then for each of these, we can get access to the uuid from the result and add to the browsers data mirror
      # simulating this action...
#      console.log('simulating....')
      #nodelist[n.getUuid()] = JSON.parse(dummy_data)
#      nodelist['fake_uuid1'] = JSON.parse(dummy_data)
#      nodelist.e = {};
#      nodelist['fake_uuid2'] = JSON.parse(dummy_data)        
#      nodelist['fake_uuid3'] = JSON.parse(dummy_data)  
      
      
      #simulating some connections
      
#      edgelist['fake_edgeid_1'] = {n1: 'fake_uuid1', n2: 'fake_uuid2'}
#      node_list['fake_uuid1'].e.push('fake_edgeid_1')
      
      
      #node_list['fake_uuid2'].connections.push('fake_edgeid_1')
      
      
      #edgelist['fake_edgeid_2'] = {n1: 'fake_uuid2', n2: 'fake_uuid3'}
#      console.log(nodelist)
#      console.log(edgelist)
            
     
            
            #console.log(n)
         #)
          #n.getAttributes().then((a) ->
          #  console.log('--- get attributes ---')
          #  test_properties = {color: 'blue', size: '10'}
          #  nodelist["test_id"] = a #a should be replaced with returned attributes
      
          #,(e)-> console.log(e)) # eo getAttributes cb
        #)
       
#       ,(e)-> console.log(e)) #eo createNode cb
#         
#         
#       return g


    )#get explicit graph
  )#eo start
#  )      
  return


