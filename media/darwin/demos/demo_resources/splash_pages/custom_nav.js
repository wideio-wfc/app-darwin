$('document').ready(function(){
	var $darwin_link = $('.darwin-link');
	
	$darwin_link.mouseover(function(){
		var source = $darwin_link.attr("src");
		var new_source = source.replace('link', 'hover');
		$darwin_link.attr("src", new_source);
	});
	$darwin_link.mouseout(function(){
		var source = $darwin_link.attr("src");
		var new_source = source.replace('hover', 'link');
		$darwin_link.attr("src", new_source);
	});
})